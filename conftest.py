import pytest


@pytest.fixture
def create_players():
    return [{"id": 1, "name": "p1", "color": "yellow"}, {"id": 2, "name": "p2", "color": "red"}]

