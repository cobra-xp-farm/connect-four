from functools import partial
from typing import Callable, Dict, Any, Tuple
from toolz.dicttoolz import assoc
from toolz.functoolz import compose_left
from connect_four._util import coordinates_to_index


def create_game(
    players: list, ncols: int = 7, nrows: int = 6, first_player_strategy: Callable = lambda players: players[0]
) -> Dict[str, Any]:
    return {
        "board": [("empty",) for _ in range(ncols * nrows)],
        "current_player": first_player_strategy(players=players),
        "cols": ncols,
        "rows": nrows,
        "players": players,
        "msg": None,
        "is_end": False,
    }


def get_disc_fall_sequence(game, column):
    nrows = game.get('rows')
    ncols = game.get('cols')
    field_to_place = ncols * nrows
    i = 1
    while field_to_place > column:
        field_to_place = ncols * (nrows - i) + column - 1
        i += 1
        yield field_to_place


def __maybe_place_disc_at_index(game,field_to_place) -> Tuple[dict, bool]:
    board = game.get('board')
    player_color = game.get('current_player').get('color')
    if board[field_to_place] == ('empty',):
        new_board = board[:field_to_place] + [(player_color,)] + board[field_to_place + 1 :]
        new_game = assoc(game, "board", new_board)
        return new_game, True
    return game, False
    

def place_disc(game, column) -> Dict[str, Any]:
    ncols = game.get('cols')
    if ncols < column:
        return assoc(game, "msg", "Invalid move")

    first_row_index=coordinates_to_index(1, column, (game['rows'], game['cols']))
    if game.get('board')[first_row_index] != ('empty',):
        return assoc(game, "msg", "Column is full")

    for idx in get_disc_fall_sequence(game,column):
        game, is_placed = __maybe_place_disc_at_index(game,idx)
        if is_placed:
            return game
    return game


def switch_players(game):
    players = game.get('players')
    curent_player_index = players.index(game.get("current_player"))
    return assoc(game, "current_player", players[(curent_player_index + 1) % len(players)])


def make_turn(game, column):
    return compose_left(partial(place_disc, column=column), switch_players)(game)


def current_player_won(game):
    # Written by ChatGPT, I couldn't bother
    num_rows = game.get('rows')
    num_cols = game.get('cols')
    num_connect = 4
    board = game.get('board')
    player_disc = (game.get('current_player').get('color'), )

    for col in range(num_cols):
        count = 0
        for row in range(num_rows):
            idx = coordinates_to_index(row+1, col+1, (num_rows, num_cols))
            if board[idx] == player_disc:
                count +=1
            else:
                count = 0
                
            print(board[idx])
            print(count)
            
            if count == num_connect:
                return True
    
    for row in range(num_rows):
        count = 0
        for col in range(num_cols):   
            idx = coordinates_to_index(row+1, col+1, (num_rows, num_cols))
            if board[idx] == player_disc:
                count +=1
            else:
                count = 0
                                
            print(board[idx])
            print(count)
                
            if count == num_connect:
                return True
    
    return False
            

def evaluate_game(game):
    if current_player_won(game):
        game = assoc(game, "msg", f"{game.get('current_player').get('name')} won")
        game = assoc(game, "is_end", True)
    return game