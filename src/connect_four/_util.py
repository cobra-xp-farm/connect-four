from typing import Tuple


def coordinates_to_index(row:int, column:int, board_size:Tuple[int, int]):
    nrows, ncols = board_size
    return ncols*(row-1) + column -1