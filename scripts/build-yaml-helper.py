from getpass import getpass
from typing import Dict, List
import yaml
import requests
import os


def has_unset_options(config: Dict[str, str], options: List[str]) -> bool:
    return any([config.get(option) == 'undefined' for option in options])


def main():

    input(
        "Open https://pages.github.ibm.com/cio-ci-cd/documentation/python-and-cirrus/ and follow the steps until step 4. (Press `enter` to continue)"
    )

    with open(os.path.join(os.path.dirname(__file__), '../build.yml'), 'r') as f:
        config = yaml.safe_load(f)

    if has_unset_options(config, ['ealImapNumber']):
        config['ealImapNumber'] = input('Provide an EAL number\n> ') or 'undefined'

    build_config: dict = config['build']['config'].copy()

    github_access_token = getpass(
        'Provide a GitHub access token\nHint: https://github.ibm.com/settings/tokens/new?scopes=repo,read:user&description=cio-ci-cd\n> '
    ).strip()
    github_org_name = input('Name of the GitHub organization\n> ').strip()
    github_repo_name = input('Name of the GitHub repository\n> ').strip()

    # Required for pipeline
    cirrus_pipeline_username = None
    cirrus_pipeline_password = None
    cirrus_api_username = None
    cirrus_api_password = None

    # Required for private deps

    artifactory_repo_url = None
    artifactory_repo_name = None
    artifactory_username = None
    artifactory_api_key = None

    if has_unset_options(
        build_config, ['cirrus-pipeline-username', 'cirrus-pipeline-password', 'cirrus-api-username', 'cirrus-api-password']
    ):
        cirrus_pipeline_username = input(
            'Provide Cirrus pipeline username\nHint: Provided when BYOI pipeline is created or found in Credentials tab of a project in Cirrus\n> '
        ).strip()
        cirrus_pipeline_password = getpass(
            'Provide Cirrus pipeline password\nHint: Provided when BYOI pipeline is created or found in Credentials tab of a project in Cirrus\n> '
        ).strip()

        cirrus_api_username = input('Provide Cirrus API key username\nHint: https://cirrus.ibm.com/user\n> ').strip()
        cirrus_api_password = getpass('Provide Cirrus API key password\nHint: https://cirrus.ibm.com/user\n> ').strip()

        result = requests.post(
            f'https://adopter-service-prod.dal1a.cirrus.ibm.com/v1/{github_org_name}/{github_repo_name}/encrypt_all',
            json={
                "githubAccessToken": github_access_token,
                "plainTexts": {
                    'cirrus-api-username': cirrus_api_username,
                    'cirrus-api-password': cirrus_api_password,
                    'cirrus-pipeline-username': cirrus_pipeline_username,
                    'cirrus-pipeline-password': cirrus_pipeline_password,
                },
            },
        )

        encrypted = result.json()
        build_config.update(encrypted)

    if has_unset_options(build_config, ['private-dependencies']):
        input(
            'Related documentation: https://pages.github.ibm.com/cio-ci-cd/documentation/python-and-cirrus/#private-dependencies (Press `enter` to continue)'
        )
        artifactory_repo_url = input('Provide an URL of an artifactory repository\n> ').strip()
        artifactory_repo_name = input('Provide a name of an artifactory repository\n> ').strip()
        artifactory_username = input(
            'Provide an URL a username for artifactory\nHint: https://na.artifactory.swg-devops.com/ui/admin/artifactory/user_profile\n> '
        ).strip()
        artifactory_api_key = getpass(
            'Provide an URL an API key for artifactory\nHint: https://na.artifactory.swg-devops.com/ui/admin/artifactory/user_profile\n> '
        ).strip()

        os.popen(f'poetry source add {artifactory_repo_name}  {artifactory_repo_url}')

        result = requests.post(
            f'https://adopter-service-prod.dal1a.cirrus.ibm.com/v1/{github_org_name}/{github_repo_name}/encrypt_all',
            json={
                "githubAccessToken": github_access_token,
                "plainTexts": {
                    'private-dependencies': f'{artifactory_repo_url};;{artifactory_repo_name};;{artifactory_username};;{artifactory_api_key}',
                },
            },
        )

        encrypted = result.json()
        build_config.update(encrypted)

    config['build']['config'] = build_config

    with open(os.path.join(os.path.dirname(__file__), '../build.yml'), 'w') as f:
        yaml.dump(config, f)

    print('Executed successfully! Written to `build.yaml`')


if __name__ == '__main__':
    main()
