# Event storming connect 4

E1: Player starts a game

E2: Player sees 6x7 board

E3: Player chooses a column to place his disc

E11: Players switch rounds

E4: The disc falls to the bottom of the board

E5: If the column already has some discs, the disc falls on top of the previous disc

E6: Player wins if 4 of his discs are connected vertically

E7: Player wins if 4 of his discs are connected horizontally

E8: Player wins if 4 of his discs are connected diagonally

E9: Player loses if another Player wins

E10: It is a tie if there is no more space for discs

Domain language:

- Rounds
- Player
- Board
  - Disc
    - Color
      - Red
      - Yellow
  - Column
    - Row
      -Field
