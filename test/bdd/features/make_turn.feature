Scenario: Player makes a turn on empty board
    Given a game is started
    And current player is yellow
    When current player makes turn on 2nd column
    Then his disc is placed at 6th row and 2nd column
    And it is red player's turn

Scenario: Player makes a turn on non empty board
    Given a game is started
    And current player is yellow
    And 2nd column and 6th row field has a red disc already placed
    When current player makes turn on 2nd column
    Then his disc is placed at 5th row and 2nd column
    And it is red player's turn
