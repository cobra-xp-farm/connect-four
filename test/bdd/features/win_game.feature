Scenario: Player wins vertically
    Given a game is started
    And current player is red
    And 2nd column has 3 red discs already placed
    When current player makes his turn placing his disc in 2nd column
    Then red player wins the game
    And the game is over

# Scenario: Game is tie
#     Given a game is started
#     And current player is red
#     And there is only 1 field left
#     And no 3 discs are connected
#     When a player places his disc
#     Then it is a tie
#     And the game is over