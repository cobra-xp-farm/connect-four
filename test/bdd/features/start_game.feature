Scenario: Game is started
    When a Connect-four game is created
    Then a board is created
    And the board has 7 columns
    And the board has 6 rows
    And the board has no discs

Scenario: Player's turn is determined
    When board is created
    Then starting Player is assigned 