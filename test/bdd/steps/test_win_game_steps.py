from pytest_bdd import scenarios, given, when, then
from connect_four import create_game, place_disc, make_turn

scenarios("win_game.feature")

@given("a game is started", target_fixture="game")
def a_game_is_started(create_players):
    players = create_players
    return create_game(players)

@given("current player is red", target_fixture="game")
def current_player_is_red(game):
    game['current_player'] = game['players'][1]
    return game

@given("2nd column has 3 red discs already placed")
def second_column_has_3_red_discs_already_placed(game):
    for _ in range(3):
        game = place_disc(game, 2)
    return game

@when("current player makes his turn placing his disc in 2nd column", target_fixture="game")
def current_player_places_his_disc_in_2nd_column(game):
    return make_turn(game, 2)

    
@then("red player wins the game")
def red_player_wins_the_game(game):
    assert game['msg']== 'Red player won'

@then("the game is over")
def the_game_is_over(game):
    assert game["is_end"] == True