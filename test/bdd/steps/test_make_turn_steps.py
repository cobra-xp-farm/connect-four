from pytest_bdd import scenarios, given, when, then
from connect_four import create_game, make_turn
from connect_four._util import coordinates_to_index
from whatever import _

scenarios("make_turn.feature")

# Scenario: Player makes a turn
#     Given a game is started
#     When current player chooses a column
#     Then his disc is placed at the bottom row fo that column


@given("a game is started", target_fixture="game")
def a_game_is_started(create_players):
    players = create_players
    return create_game(players)


@given("current player is yellow", target_fixture="game")
def current_player_is_yellow(create_players, game):
    yellow_player = list(filter(_["color"] == "yellow", create_players))[0]
    game["current_player"] = yellow_player
    return game


@when("current player makes turn on 2nd column", target_fixture="game")
def current_player_makes_turn(game):
    new_game = make_turn(game=game, column=2)
    return new_game


@then("his disc is placed at 6th row and 2nd column")
def his_disc_is_placed(game):
    board = game.get("board")
    field = coordinates_to_index(6, 2, (game.get('rows'), game.get('cols')) )
    assert board[field] == ("yellow",)


@given("2nd column and 6th row field has a red disc already placed", target_fixture="game")
def disc_is_placed_on_6_row_2_col(game):
    board = game.get("board")
    field = coordinates_to_index(6, 2, (game.get('rows'), game.get('cols')) )
    board[field] = ("red",)
    game["board"] = board
    return game


@when("current player makes turn on 2nd column", target_fixture="game")
def current_player_makes_turn_on_2_col(game):
    return make_turn(game, 2)


@then("his disc is placed at 5th row and 2nd column")
def his_disc_is_placed_on_top_of_another_disc(game):

    board = game.get("board")
    field = coordinates_to_index(5, 2, (game.get('rows'), game.get('cols')) )
    assert board[field] == ("yellow",)


@then("it is red player's turn")
def it_is_red_players_turn(game):
    assert game.get('current_player').get("color") == "red"
