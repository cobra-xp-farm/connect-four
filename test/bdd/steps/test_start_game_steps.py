from pytest_bdd import scenarios, given, when, then
from connect_four import create_game
from whatever import _

scenarios("start_game.feature")


@when("a Connect-four game is created", target_fixture="game")
def connect_four_game_is_created(create_players):
    players = create_players
    return create_game(players)


@then('a board is created')
def a_board_is_created(game):
    assert game.get('board') is not None


@then('the board has 7 columns')
def the_board_has_7_columns(game):
    assert len(game.get('board')) % 7 == 0


@then('the board has 6 rows')
def board_has_6_rows(game):
    assert len(game.get('board')) % 6 == 0


@then('the board has no discs')
def board_has_no_discs(game):
    assert all(map(_[0] == "empty", game.get('board')))


@when('board is created', target_fixture='game')
def board_is_created(create_players):
    players = create_players
    return create_game(players)


@then('starting Player is assigned')
def starting_player_is_assigned(game):
    assert game.get('current_player') is not None
