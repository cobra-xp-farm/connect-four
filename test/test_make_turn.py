from connect_four import place_disc, switch_players, make_turn, create_game
import pytest
from connect_four._util import coordinates_to_index


@pytest.mark.parametrize(
    "game, column, new_game",
    [
        (
            {
                "board": [("empty",), ("empty",), ("empty",)],
                "current_player": {"id": 1, "name": "p1", "color": "yellow"},
                "cols": 3,
                "rows": 1,
            },
            2,
            {
                "board": [("empty",), ("yellow",), ("empty",)],
                "current_player": {"id": 1, "name": "p1", "color": "yellow"},
                "cols": 3,
                "rows": 1,
            },
        ),
        (
            {
                "board": [("empty",), ("empty",), ("empty",), ("red",)],
                "current_player": {"id": 1, "name": "p1", "color": "yellow"},
                "cols": 2,
                "rows": 2,
            },
            2,
            {
                "board": [("empty",), ("yellow",), ("empty",), ("red",)],
                "current_player": {"id": 1, "name": "p1", "color": "yellow"},
                "cols": 2,
                "rows": 2,
            },
        ),
    ],
)
def place_disc_test(game, column, new_game):
    assert place_disc(game=game, column=column) == new_game


def place_disc_empty_board_test(create_players):
    game = create_game(create_players)
    new_game = place_disc(game, 2)
    field = coordinates_to_index(6, 2, (game['rows'], game['cols']))
    assert new_game.get('board', [])[field] == ('yellow',)


def place_disc_column_not_empty_test(create_players):
    game = create_game(create_players)
    new_game = place_disc(game, 2)
    new_game = place_disc(new_game, 2)
    field = coordinates_to_index(5, 2, (game['rows'], game['cols']))
    assert new_game.get("board",[])[field] == ('yellow',)
    

def place_disc_column_full_test(create_players):
    game = create_game(create_players)
    
    for _ in range(game['rows']):
        game = place_disc(game, 2)
    new_game = place_disc(game, 2)
    
    assert game['msg'] == None
    assert new_game['msg'] == "Column is full"


def place_disc_column_out_of_range_test(create_players):
    game = create_game(create_players)
    new_game = place_disc(game, 8)
    assert new_game['msg'] == "Invalid move"


def switch_players_test(create_players):
    game = create_game(create_players)
    new_game = switch_players(game=game)
    assert new_game.get("current_player") == new_game.get("players", [])[1]


def make_turn_test(create_players):
    game = create_game(create_players)

    new_game = make_turn(game=game, column=2)

    field = coordinates_to_index(6, 2, (game['rows'], game['cols']))
    assert new_game.get("current_player") == new_game.get("players", [])[1]
    assert new_game.get("board", [])[field] == ('yellow',)
