from connect_four import evaluate_game, create_game, place_disc

def test_game_finished_with_win_vertically(create_players):
    game = create_game(create_players)
    for _ in range(4):
        game = place_disc(game, 2)

    new_game = evaluate_game(game)
    
    assert new_game['msg'] == "p1 won"
    assert new_game["is_end"] == True
    
def test_game_finished_with_win_horizontally(create_players):
    game = create_game(create_players)
    for i in range(4):
        game = place_disc(game, i+1)

    new_game = evaluate_game(game)
    
    assert new_game['msg'] == "p1 won"
    assert new_game["is_end"] == True