import pytest
from connect_four import create_game


@pytest.mark.parametrize(('ncols', 'nrows', 'result'), [(3, 5, 15), (5, 5, 25), (2, 2, 4)])
def test_create_game(ncols, nrows, result, create_players):
    players = create_players
    game = create_game(players=players, ncols=ncols, nrows=nrows)

    assert len(game['board']) == result


def test_determine_first_player(create_players):
    players = create_players
    next_game_state = create_game(players, ncols=7, nrows=6)
    assert next_game_state['current_player'] == players[0]
