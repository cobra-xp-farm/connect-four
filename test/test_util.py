from connect_four import _util



def coordinates_to_index_test():
    assert _util.coordinates_to_index(6, 2, (6, 7)) == 36
    assert _util.coordinates_to_index(5, 2, (6, 7)) == 29
    assert _util.coordinates_to_index(4, 5, (6, 7)) == 25
    